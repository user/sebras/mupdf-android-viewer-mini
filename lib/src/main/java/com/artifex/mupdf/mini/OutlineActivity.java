package com.artifex.mupdf.mini;

import com.artifex.mupdf.fitz.*;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.io.Serializable;
import java.util.ArrayList;

public class OutlineActivity extends ListActivity
{
	private final String APP = "MuPDF";

	public static class Item implements Serializable {
		public String title;
		public String uri;
		public Location page;
		public Item(String title, String uri, Location page) {
			this.title = title;
			this.uri = uri;
			this.page = page;
		}
		public String toString() {
			return title;
		}
	}

	protected ArrayAdapter<Item> adapter;

	private boolean before(Location a, Location b) {
		return (a.chapter < b.chapter) || (a.chapter == b.chapter && a.page < b.page);
	}

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

		adapter = new ArrayAdapter<Item>(this, android.R.layout.simple_list_item_1);
		setListAdapter(adapter);

		Bundle bundle = getIntent().getExtras();
		Location currentPage = new Location(bundle.getInt("CHAPTER"), bundle.getInt("PAGE"));
		ArrayList<Item> outline = (ArrayList<Item>)bundle.getSerializable("OUTLINE");
		int found = -1;
		for (int i = 0; i < outline.size(); ++i) {
			Item item = outline.get(i);
			Log.i(APP, "checking if " + item.page + " is before " + currentPage);
			if (found < 0 && before(currentPage, item.page))
			{
				found = i;
				Log.i(APP, "found is now " + found);
			}
			adapter.add(item);
		}
		Log.i(APP, "found determined to be " + found);
		if (found >= 0)
		{
			Log.i(APP, "set selection to " + found);
			setSelection(found);
		}
	}

	protected void onListItemClick(ListView l, View v, int position, long id) {
		Item item = adapter.getItem(position);
		Intent intent = new Intent(OutlineActivity.this, DocumentActivity.class);
		Bundle bundle = new Bundle();
		bundle.putInt("CHAPTER", item.page.chapter);
		bundle.putInt("PAGE", item.page.page);
		intent.putExtras(bundle);
		setResult(RESULT_OK, intent);
		finish();
	}
}
