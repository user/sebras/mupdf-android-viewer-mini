package com.artifex.mupdf.mini;

import android.app.Activity;
import android.util.Log;
import android.widget.Toast;

import java.io.StringWriter;
import java.io.PrintWriter;
import java.util.concurrent.LinkedBlockingQueue;

public class Worker implements Runnable
{
	private final String APP = "MuPDF-Worker";

	public static class Task implements Runnable {
		public String what;
		public void work() {} /* The 'work' method will be executed on the background thread. */
		public void run() {} /* The 'run' method will be executed on the UI thread. */
		public String toString() {
			StringBuilder sb = new StringBuilder("Task(");
			sb.append(what);
			sb.append(")");
			return sb.toString();
		}
	}

	protected Activity activity;
	protected LinkedBlockingQueue<Task> queue;
	protected boolean alive;

	public Worker(Activity act) {
		activity = act;
		queue = new LinkedBlockingQueue<Task>();
	}

	public void start() {
		alive = true;
		new Thread(this).start();
	}

	public void stop() {
		alive = false;
	}

	public void add(String what, Task task) {
		try {
			task.what = what;
			Log.i(APP, task + ": enqueuing task");
			queue.put(task);
			Log.i(APP, task + ": enqueued");
		} catch (InterruptedException x) {
			Log.e(APP, task + ": " + x.getMessage());
		}
	}

	public void run() {
		while (alive) {
			Task task = null;
			try {
				task = queue.take();
				Log.i(APP, task + ": running task work ");
				task.work();
				Log.i(APP, task + ": ran task work, scheduling on UI thread");
				activity.runOnUiThread(task);
				Log.i(APP, task + ": scheduled");
			} catch (final Throwable x) {
				if (x.getMessage() != null)
					Log.e(APP, task + ": " + x.getMessage());
				else
					Log.e(APP, task + ": " + x.toString());
				activity.runOnUiThread(new Runnable() {
					public void run() {
						Toast.makeText(activity, x.getMessage(), Toast.LENGTH_SHORT).show();
					}
				});
			}
		}
	}
}
