package com.artifex.mupdf.mini;

import com.artifex.mupdf.fitz.*;
import com.artifex.mupdf.fitz.android.*;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.FileUriExposedException;
import android.os.ParcelFileDescriptor;
import android.provider.OpenableColumns;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.PopupMenu;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Stack;

public class DocumentActivity extends Activity
{
	private final String APP = "MuPDF-DA";

	public final int NAVIGATE_REQUEST = 1;

	protected Worker worker;
	protected SharedPreferences prefs;

	protected Document doc;

	protected String key;
	protected String mimetype;
	protected SeekableInputStream stream;
	protected byte[] buffer;

	protected boolean returnToLibraryActivity;
	protected boolean hasLoaded;
	protected boolean isReflowable;
	protected boolean fitPage;
	protected String title;
	protected ArrayList<OutlineActivity.Item> flatOutline;
	protected float layoutW, layoutH, layoutEm;
	protected float displayDPI;
	protected int canvasW, canvasH;
	protected float pageZoom;

	protected View currentBar;
	protected PageView pageView;
	protected View actionBar;
	protected TextView titleLabel;
	protected View searchButton;
	protected View searchBar;
	protected EditText searchText;
	protected View searchCloseButton;
	protected View searchBackwardButton;
	protected View searchForwardButton;
	protected View zoomButton;
	protected View layoutButton;
	protected PopupMenu layoutPopupMenu;
	protected View outlineButton;
	protected View navigationBar;
	protected TextView pageLabel;
	protected SeekBar pageSeekbar;

	protected int chapterCount;
	protected int pageCount;
	protected int pendingCurrentPage;
	protected Location currentPage;
	protected Location searchHitPage;
	protected String searchNeedle;
	protected boolean stopSearch;
	protected Stack<Location> history;
	protected boolean wentBack;

	private String toHex(byte[] digest) {
		StringBuilder builder = new StringBuilder(2 * digest.length);
		for (byte b : digest)
			builder.append(String.format("%02x", b));
		return builder.toString();
	}

	private void openInput(Uri uri, long size, String mimetype) throws IOException {
		ContentResolver cr = getContentResolver();

		Log.i(APP, "Opening document " + uri);

		InputStream is = cr.openInputStream(uri);
		byte[] buf = null;
		int used = -1;
		try {
			final int limit = 8 * 1024 * 1024;
			if (size < 0) { // size is unknown
				buf = new byte[limit];
				used = is.read(buf);
				boolean atEOF = is.read() == -1;
				if (used < 0 || (used == limit && !atEOF)) // no or partial data
					buf = null;
			} else if (size <= limit) { // size is known and below limit
				buf = new byte[(int) size];
				used = is.read(buf);
				if (used < 0 || used < size) // no or partial data
					buf = null;
			}
			if (buf != null && buf.length != used) {
				byte[] newbuf = new byte[used];
				System.arraycopy(buf, 0, newbuf, 0, used);
				buf = newbuf;
			}
		} catch (OutOfMemoryError e) {
			buf = null;
		} finally {
			is.close();
		}

		if (buf != null) {
			Log.i(APP, "  Opening document from memory buffer of size " + buf.length);
			buffer = buf;
		} else {
			Log.i(APP, "  Opening document from stream");
			stream = new ContentInputStream(cr, uri, size);
		}
	}

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

		DisplayMetrics metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);
		displayDPI = metrics.densityDpi;

		setContentView(R.layout.document_activity);
		actionBar = findViewById(R.id.action_bar);
		searchBar = findViewById(R.id.search_bar);
		navigationBar = findViewById(R.id.navigation_bar);

		currentBar = actionBar;

		Uri uri = getIntent().getData();
		mimetype = getIntent().getType();

		if (uri == null) {
			Toast.makeText(this, "No document uri to open", Toast.LENGTH_SHORT).show();
			return;
		}

		returnToLibraryActivity = getIntent().getIntExtra(getComponentName().getPackageName() + ".ReturnToLibraryActivity", 0) != 0;

		key = uri.toString();

		Log.i(APP, "OPEN URI " + uri.toString());
		Log.i(APP, "  MAGIC (Intent) " + mimetype);

		title = "";
		long size = -1;
		Cursor cursor = null;

		try {
			cursor = getContentResolver().query(uri, null, null, null, null, null);
			if (cursor != null && cursor.moveToFirst()){
				int idx;

				idx = cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
				if (idx >= 0 && cursor.getType(idx) == Cursor.FIELD_TYPE_STRING)
					title = cursor.getString(idx);

				idx = cursor.getColumnIndex(OpenableColumns.SIZE);
				if (idx >= 0 && cursor.getType(idx) == Cursor.FIELD_TYPE_INTEGER)
					size = cursor.getLong(idx);

				if (size == 0)
					size = -1;
			}
		} catch (Exception x) {
			// Ignore any exception and depend on default values for title
			// and size (unless one was decoded
		} finally {
			if (cursor != null)
				cursor.close();
		}

		Log.i(APP, "  NAME " + title);
		Log.i(APP, "  SIZE " + size);

		if (mimetype == null || mimetype.equals("application/octet-stream")) {
			mimetype = getContentResolver().getType(uri);
			Log.i(APP, "  MAGIC (Resolver) " + mimetype);
		}
		if (mimetype == null || mimetype.equals("application/octet-stream")) {
			mimetype = title;
			Log.i(APP, "  MAGIC (Filename) " + mimetype);
		}

		try {
			openInput(uri, size, mimetype);
		} catch (Exception x) {
			Log.e(APP, x.toString());
			String text = x.getMessage();
			if (text == null)
				text = x.getClass().getName();
			Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
		}

		titleLabel = (TextView)findViewById(R.id.title_label);
		titleLabel.setText(title);

		history = new Stack<Location>();

		worker = new Worker(this);
		worker.start();

		prefs = getPreferences(Context.MODE_PRIVATE);
		layoutEm = prefs.getFloat("layoutEm", 8);
		fitPage = prefs.getBoolean("fitPage", false);
		pendingCurrentPage = prefs.getInt(key, 0);
		searchHitPage = null;
		hasLoaded = false;

		pageView = (PageView)findViewById(R.id.page_view);
		pageView.setActionListener(this);

		pageLabel = (TextView)findViewById(R.id.page_label);
		pageLabel.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				Toast.makeText(DocumentActivity.this, pageLabel.getText(), Toast.LENGTH_LONG).show();
			}
		});
		pageSeekbar = (SeekBar)findViewById(R.id.page_seekbar);
		pageSeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
			public int newProgress = -1;
			public void onProgressChanged(SeekBar seekbar, int progress, boolean fromUser) {
				if (fromUser) {
					newProgress = progress;
					pageLabel.setText((progress+1) + " / " + pageCount());
				}
			}
			public void onStartTrackingTouch(SeekBar seekbar) {}
			public void onStopTrackingTouch(SeekBar seekbar) {
				gotoPage(newProgress);
			}
		});

		searchButton = findViewById(R.id.search_button);
		searchButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				showSearch();
			}
		});
		searchText = (EditText)findViewById(R.id.search_text);
		searchText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				if (actionId == EditorInfo.IME_NULL && event.getAction() == KeyEvent.ACTION_DOWN) {
					search(1);
					return true;
				}
				if (actionId == EditorInfo.IME_ACTION_SEARCH) {
					search(1);
					return true;
				}
				return false;
			}
		});
		searchText.addTextChangedListener(new TextWatcher() {
			public void afterTextChanged(Editable s) {}
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				resetSearch();
			}
		});
		searchCloseButton = findViewById(R.id.search_close_button);
		searchCloseButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				hideSearch();
			}
		});
		searchBackwardButton = findViewById(R.id.search_backward_button);
		searchBackwardButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				search(-1);
			}
		});
		searchForwardButton = findViewById(R.id.search_forward_button);
		searchForwardButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				search(1);
			}
		});

		outlineButton = findViewById(R.id.outline_button);
		outlineButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				Intent intent = new Intent(DocumentActivity.this, OutlineActivity.class);
				Bundle bundle = new Bundle();
				bundle.putInt("CHAPTER", currentPage.chapter);
				bundle.putInt("PAGE", currentPage.page);
				bundle.putSerializable("OUTLINE", flatOutline);
				intent.putExtras(bundle);
				startActivityForResult(intent, NAVIGATE_REQUEST);
			}
		});

		zoomButton = findViewById(R.id.zoom_button);
		zoomButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				fitPage = !fitPage;
				loadPage();
			}
		});

		layoutButton = findViewById(R.id.layout_button);
		layoutPopupMenu = new PopupMenu(this, layoutButton);
		layoutPopupMenu.getMenuInflater().inflate(R.menu.layout_menu, layoutPopupMenu.getMenu());
		layoutPopupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
			public boolean onMenuItemClick(MenuItem item) {
				float oldLayoutEm = layoutEm;
				int id = item.getItemId();
				if (id == R.id.action_layout_6pt) layoutEm = 6;
				else if (id == R.id.action_layout_7pt) layoutEm = 7;
				else if (id == R.id.action_layout_8pt) layoutEm = 8;
				else if (id == R.id.action_layout_9pt) layoutEm = 9;
				else if (id == R.id.action_layout_10pt) layoutEm = 10;
				else if (id == R.id.action_layout_11pt) layoutEm = 11;
				else if (id == R.id.action_layout_12pt) layoutEm = 12;
				else if (id == R.id.action_layout_13pt) layoutEm = 13;
				else if (id == R.id.action_layout_14pt) layoutEm = 14;
				else if (id == R.id.action_layout_15pt) layoutEm = 15;
				else if (id == R.id.action_layout_16pt) layoutEm = 16;
				if (oldLayoutEm != layoutEm)
					relayoutDocument();
				return true;
			}
		});
		layoutButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				layoutPopupMenu.show();
			}
		});
	}

	public boolean onKeyUp(int keyCode, KeyEvent event) {
		switch (keyCode) {
		case KeyEvent.KEYCODE_PAGE_UP:
		case KeyEvent.KEYCODE_COMMA:
		case KeyEvent.KEYCODE_B:
			goBackward();
			return true;
		case KeyEvent.KEYCODE_PAGE_DOWN:
		case KeyEvent.KEYCODE_PERIOD:
		case KeyEvent.KEYCODE_SPACE:
			goForward();
			return true;
		case KeyEvent.KEYCODE_M:
			history.push(currentPage);
			return true;
		case KeyEvent.KEYCODE_T:
			if (!history.empty()) {
				currentPage = history.pop();
				loadPage();
			}
			return true;
		}
		return super.onKeyUp(keyCode, event);
	}

	public void onPageViewSizeChanged(int w, int h) {
		pageZoom = 1;
		canvasW = w;
		canvasH = h;
		layoutW = canvasW * 72 / displayDPI;
		layoutH = canvasH * 72 / displayDPI;
		if (!hasLoaded) {
			hasLoaded = true;
			openDocument();
		} else if (isReflowable) {
			relayoutDocument();
		} else {
			loadPage();
		}
	}

	public void onPageViewZoomChanged(float zoom) {
		if (zoom != pageZoom) {
			pageZoom = zoom;
			loadPage();
		}
	}

	protected synchronized void setCurrentPage(int page) {
		pageSeekbar.setProgress(page);
	}

	protected synchronized void setPageCount(int pages) {
		Log.i(APP, "setPageCount(" + pages + ")");
		if (pages == 0) {
			pageCount = 0;
			pageSeekbar.setMax(0);
		} else if (pages > pageCount) {
			pageCount = pages;
			pageSeekbar.setMax(pages - 1);
		} else
			Log.i(APP, "ignoring new maximum since it is smaller than existing one!");
	}

	protected synchronized int pageCount() {
		return pageCount;
	}

	protected void openDocument() {
		worker.add("openDocument", new Worker.Task() {
			boolean needsPassword;
			Location startingPage;
			public void work() {
				Log.i(APP, "open document");
				if (buffer != null)
					doc = Document.openDocument(buffer, mimetype);
				else
					doc = Document.openDocument(stream, mimetype);
				startingPage = doc.locationFromPageNumber(pendingCurrentPage);
				needsPassword = doc.needsPassword();
			}
			public void run() {
				currentPage = startingPage;
				if (needsPassword)
					askPassword(R.string.dlog_password_message);
				else
					loadDocument();
			}
		});
	}

	protected void askPassword(int message) {
		final EditText passwordView = new EditText(this);
		passwordView.setInputType(EditorInfo.TYPE_TEXT_VARIATION_PASSWORD);
		passwordView.setTransformationMethod(PasswordTransformationMethod.getInstance());

		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(R.string.dlog_password_title);
		builder.setMessage(message);
		builder.setView(passwordView);
		builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				checkPassword(passwordView.getText().toString());
			}
		});
		builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				finish();
			}
		});
		builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
			public void onCancel(DialogInterface dialog) {
				finish();
			}
		});
		builder.create().show();
	}

	protected void checkPassword(final String password) {
		worker.add("checkPassword", new Worker.Task() {
			boolean passwordOkay;
			public void work() {
				Log.i(APP, "check password");
				passwordOkay = doc.authenticatePassword(password);
			}
			public void run() {
				if (passwordOkay)
					loadDocument();
				else
					askPassword(R.string.dlog_password_retry);
			}
		});
	}

	public void onPause() {
		super.onPause();
		if (prefs != null) {
			SharedPreferences.Editor editor = prefs.edit();
			editor.putFloat("layoutEm", layoutEm);
			editor.putBoolean("fitPage", fitPage);
			editor.putInt(key, doc.pageNumberFromLocation(currentPage));
			editor.apply();
		}
	}

	public void onBackPressed() {
		if (history.empty()) {
			super.onBackPressed();
			if (returnToLibraryActivity) {
				Intent intent = getPackageManager().getLaunchIntentForPackage(getComponentName().getPackageName());
				startActivity(intent);
			}
		} else {
			currentPage = history.pop();
			loadPage();
		}
	}

	public void onActivityResult(int request, int result, Intent data) {
		if (request == NAVIGATE_REQUEST && result == RESULT_OK) {
			Bundle bundle = data.getExtras();
			Location loc = new Location(bundle.getInt("CHAPTER"), bundle.getInt("PAGE"));
			gotoPage(loc);
		}
	}

	protected void showKeyboard() {
		InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
		if (imm != null)
			imm.showSoftInput(searchText, 0);
	}

	protected void hideKeyboard() {
		InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
		if (imm != null)
			imm.hideSoftInputFromWindow(searchText.getWindowToken(), 0);
	}

	protected void resetSearch() {
		stopSearch = true;
		searchHitPage = null;
		searchNeedle = null;
		pageView.resetHits();
	}

	protected void runSearch(final Location startPage, final int direction, final String needle) {
		stopSearch = false;
		worker.add("runSearch", new Worker.Task() {
			Location searchPage = startPage;
			public void work() {
				if (stopSearch || needle != searchNeedle)
					return;
				for (int i = 0; i < 9; ++i) {
					Log.i(APP, "search page " + searchPage);
					Page page = doc.loadPage(searchPage);
					Quad[][] hits = page.search(searchNeedle);
					page.destroy();
					if (hits != null && hits.length > 0) {
						searchHitPage = searchPage;
						break;
					}
					Location nextPage;
					if (direction > 0)
						nextPage = doc.nextPage(searchPage);
					else
						nextPage = doc.previousPage(searchPage);
					if (searchPage.equals(nextPage)) {
						searchPage = null;
						break;
					}
					searchPage = nextPage;
				}
			}
			public void run() {
				if (stopSearch) {
					Log.i(APP, "stopping search as requested");
					setPageLabel(currentPage);
				} else if (needle != searchNeedle) {
					Log.i(APP, "stopping search because of changed search text");
					setPageLabel(currentPage);
				} else if (currentPage.equals(searchHitPage)) {
					Log.i(APP, "current page is search hit page, reload it!");
					loadPage();
				} else if (searchHitPage != null) {
					Log.i(APP, "current page is not search hit page, load search hit page");
					history.push(currentPage);
					currentPage = searchHitPage;
					loadPage();
				} else if (searchPage != null) {
					Log.i(APP, "search not finished, continue searching");
					setPageLabel(searchPage);
					worker.add("runSearch-continuation", this);
				} else {
					Log.i(APP, "search finished, reload current page");
					setPageLabel(currentPage);
					Log.i(APP, "search not found");
					Toast.makeText(DocumentActivity.this, getString(R.string.toast_search_not_found), Toast.LENGTH_SHORT).show();
				}
			}
		});
	}

	protected void setPageLabel(Location page) {
		worker.add("setPageLabel", new Worker.Task() {
			int pageNumber;
			public void work() {
				pageNumber = doc.pageNumberFromLocation(page);
			}
			public void run() {
				int pages = pageCount();
				pageSeekbar.setProgress(pageNumber);
				if (chapterCount == 1) {
					if (pages < 0)
						pageLabel.setText(page.page + " / ?");
					else
						pageLabel.setText(page.page + " / " + pages);
				} else {
					if (pages < 0)
						pageLabel.setText(page.chapter + " / " + chapterCount + " - " + page.page + " / ?");
					else
						pageLabel.setText(page.chapter + " / " + chapterCount + " - " + page.page + " / " + pages);
				}
			}
		});
	}

	protected void search(int direction) {
		hideKeyboard();
		Location startPage;
		if (currentPage.equals(searchHitPage)) {
			if (direction > 0)
				startPage = doc.nextPage(currentPage);
			else
				startPage = doc.previousPage(currentPage);
		} else
			startPage = currentPage;
		searchHitPage = null;
		searchNeedle = searchText.getText().toString();
		if (searchNeedle.length() == 0)
			searchNeedle = null;
		if (startPage != null && searchNeedle != null)
			runSearch(startPage, direction, searchNeedle);
	}

	protected void loadDocument() {
		worker.add("loadDocument", new Worker.Task() {
			public void work() {
				try {
					Log.i(APP, "load document");
					String metaTitle = doc.getMetaData(Document.META_INFO_TITLE);
					if (metaTitle != null && !metaTitle.equals(""))
						title = metaTitle;
					isReflowable = doc.isReflowable();
					if (isReflowable) {
						Log.i(APP, "layout document");
						doc.layout(layoutW, layoutH, layoutEm);
					}
					chapterCount = doc.countChapters();
				} catch (Throwable x) {
					doc = null;
					setPageCount(0);
					currentPage = new Location(0, 0);
					throw x;
				}
			}
			public void run() {
				int pages = pageCount();
				if (currentPage.chapter >= chapterCount)
					currentPage = new Location(0, 0);
				else if (pages >= 0 && currentPage.page >= pages)
					currentPage = new Location(0, 0);
				else
				titleLabel.setText(title);
				if (isReflowable)
					layoutButton.setVisibility(View.VISIBLE);
				else
					zoomButton.setVisibility(View.VISIBLE);
				countPages();
				loadPage();
				loadOutline();
			}
		});
	}

	protected void relayoutDocument() {
		worker.add("relayoutDocument", new Worker.Task() {
			public void work() {
				try {
					long mark = doc.makeBookmark(currentPage);
					Log.i(APP, "relayout document");
					doc.layout(layoutW, layoutH, layoutEm);
					currentPage = doc.findBookmark(mark);
				} catch (Throwable x) {
					currentPage = new Location(0, 0);
					throw x;
				}
			}
			public void run() {
				countPages();
				loadPage();
				loadOutline();
			}
		});
	}

	protected void countPages() {
		worker.add("countPages", new Worker.Task() {
			int pages = 0;
			int chapter = 0;
			int chapterPages = 0;
			public void work() {
				try {
					chapterPages = doc.countPages(chapter);
				} catch (Throwable x) {
					chapterPages = 0;
					throw x;
				}
			}
			public void run() {
				chapter++;
				if (chapter < chapterCount) {
					pages += chapterPages;
					chapterPages = 0;
					setPageCount(pages);
					worker.add("countPages-continuation", this);
				} else {
					setPageCount(pages + chapterPages);
				}
			}
		});
	}

	private void loadOutline() {
		worker.add("loadOutline", new Worker.Task() {
			private void flattenOutline(Outline[] outline, String indent) {
				for (Outline node : outline) {
					if (node.title != null)
					{
						Location outlinePage = doc.resolveLink(node);
						flatOutline.add(new OutlineActivity.Item(indent + node.title, node.uri, outlinePage));
					}
					if (node.down != null)
						flattenOutline(node.down, indent + "    ");
				}
			}
			public void work() {
				Log.i(APP, "load outline");
				Outline[] outline = doc.loadOutline();
				if (outline != null) {
					flatOutline = new ArrayList<OutlineActivity.Item>();
					flattenOutline(outline, "");
				} else {
					flatOutline = null;
				}
			}
			public void run() {
				if (flatOutline != null)
					outlineButton.setVisibility(View.VISIBLE);
			}
		});
	}

	protected void loadPage() {
		final Location pageToLoad = currentPage;
		final float zoom = pageZoom;
		stopSearch = true;
		worker.add("loadPage", new Worker.Task() {
			public Bitmap bitmap;
			public Rect[] linkBounds;
			public String[] linkURIs;
			public Quad[][] hits;
			public int pageNumber;
			public void work() {
				try {
					Log.i(APP, "load page " + pageToLoad);
					Page page = doc.loadPage(pageToLoad);
					pageNumber = doc.pageNumberFromLocation(pageToLoad);
					Log.i(APP, "draw page " + pageToLoad + " zoom=" + zoom);
					Matrix ctm;
					if (fitPage)
						ctm = AndroidDrawDevice.fitPage(page, canvasW, canvasH);
					else
						ctm = AndroidDrawDevice.fitPageWidth(page, canvasW);
					Link[] links = page.getLinks();
					if (links == null)
					{
						linkBounds = new Rect[0];
						linkURIs = new String[0];
					}
					else
					{
						linkBounds = new Rect[links.length];
						linkURIs = new String[links.length];
						for (int i = 0; i < links.length; i++)
						{
							linkBounds[i] = links[i].getBounds().transform(ctm);
							linkURIs[i] = links[i].getURI();
						}
					}
					if (searchNeedle != null) {
						hits = page.search(searchNeedle);
						if (hits != null)
							for (Quad[] hit : hits)
								for (Quad chr : hit)
									chr.transform(ctm);
					}
					if (zoom != 1)
						ctm.scale(zoom);
					bitmap = AndroidDrawDevice.drawPage(page, ctm);
					Log.e(APP, bitmap.getWidth() + " x " + bitmap.getHeight());

					int bytes = bitmap.getByteCount();
					if (bytes < 1024)
						Log.e(APP, bytes + " bytes");
					else if (bytes < 1024*1024)
						Log.e(APP, (bytes/1024.0) + " Kbytes");
					else if (bytes < 1024*1024*1024)
						Log.e(APP, (bytes/1024.0/1024.0) + " Mbytes");
					else
						Log.e(APP, (bytes/1024.0/1024.0/1024.0) + " Gbytes");
				} catch (Throwable x) {
					Log.e(APP, x.toString());
				}
			}
			public void run() {
				if (bitmap != null)
					pageView.setBitmap(bitmap, zoom, wentBack, linkBounds, linkURIs, hits);
				else
					pageView.setError();
				setPageLabel(currentPage);
				setCurrentPage(pageNumber);
				wentBack = false;
			}
		});
	}

	protected void showSearch() {
		currentBar = searchBar;
		actionBar.setVisibility(View.GONE);
		searchBar.setVisibility(View.VISIBLE);
		searchBar.requestFocus();
		showKeyboard();
	}

	protected void hideSearch() {
		currentBar = actionBar;
		actionBar.setVisibility(View.VISIBLE);
		searchBar.setVisibility(View.GONE);
		hideKeyboard();
		resetSearch();
	}

	public void toggleUI() {
		if (navigationBar.getVisibility() == View.VISIBLE) {
			currentBar.setVisibility(View.GONE);
			navigationBar.setVisibility(View.GONE);
			if (currentBar == searchBar)
				hideKeyboard();
		} else {
			currentBar.setVisibility(View.VISIBLE);
			navigationBar.setVisibility(View.VISIBLE);
			if (currentBar == searchBar) {
				searchBar.requestFocus();
				showKeyboard();
			}
		}
	}

	public void goBackward() {
		Log.i(APP, "goBackward from " + currentPage);
		Location newPage = doc.previousPage(currentPage);
		Log.i(APP, "newPage " + newPage);
		if (!currentPage.equals(newPage)) {
			Log.i(APP, "not the same page, load it!");
			wentBack = true;
			currentPage = newPage;
			loadPage();
		}
	}

	public void goForward() {
		Log.i(APP, "goForward from " + currentPage);
		Location newPage = doc.nextPage(currentPage);
		Log.i(APP, "newPage " + newPage);
		if (!currentPage.equals(newPage)) {
			Log.i(APP, "not the same page, load it!");
			currentPage = newPage;
			loadPage();
		}
	}

	public void gotoPage(Location loc) {
		if (!currentPage.equals(loc)) {
			int pageNumber = doc.pageNumberFromLocation(loc);
			if (pageNumber != -1) {
				history.push(currentPage);
				currentPage = loc;
				loadPage();
			}
		}
	}

	public void gotoPage(int page) {
		gotoPage(doc.locationFromPageNumber(page));
	}

	public void gotoPage(String uri) {
		gotoPage(doc.resolveLink(uri));
	}

	public void gotoURI(String uri) {
		Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET); // FLAG_ACTIVITY_NEW_DOCUMENT in API>=21
		try {
			startActivity(intent);
		} catch (FileUriExposedException x) {
			Log.e(APP, x.toString());
			Toast.makeText(DocumentActivity.this, "Android does not allow following file:// link: " + uri, Toast.LENGTH_LONG).show();
		} catch (Throwable x) {
			Log.e(APP, x.getMessage());
			Toast.makeText(DocumentActivity.this, x.getMessage(), Toast.LENGTH_SHORT).show();
		}
	}
}
